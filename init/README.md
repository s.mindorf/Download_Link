Configurations
================================================

# nginx

Copy `download-link.conf` to your nginx configurations folder

edit the configuration

# Install Service (systemd)

Copy `uwsgi.service` into `/etc/systemd/system/`

reload systemd

`systemctl daemon-reload`

add to systemstart

`systemctl enable uwsg`

start

`systemctl start uwsgi`
