# Contribute to Download-link

Thank you for your interest in contributing to Download-link. This guide details how
to contribute to Download-link in a way that is efficient for everyone.

## Contributor license agreement

By submitting code you agree to the [GNU AGPLv3](/LICENSE)

## Security vulnerability disclosure

Please report suspected security vulnerabilities in private to
`soeren@mit42.de` or create a confidential Issue.
Please do **NOT** create publicly viewable issues for suspected security
vulnerabilities.

# How to Contribute

1. Fork the project
2. Create a feature branch
3. Code
4. Create a pull request

# We only accept pull requests if: 

* Your code can be merged w/o problems 
* It won't break existing functionality
* We like it :)

