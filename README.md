Download_link
=============

We created Download-Link because we needed a webapplication for up- and downloading files of any size with the ability to create and manage individual downloadkeys.

Features:
- Upload and manage files
- Create multiple Downloadkey for one file
- Lifetime for the Downloadkey
- Lifetime for the file itself
- Every key can be only used a limited number of times


# Get MIT42-Downloadlink

### Clone my repository

```bash 
git clone https://gitlab.com/s.mindorf/Download_Link.git
```


### Copy config.py.dist to config.py
```bash
cp config.py.dist config.py
```

### Edit `config.py` for Database settings and SECRET_KEY.

### create Downloadfolder outside your Webroot and edit `config.py`

### create your virtual environment:
```bash
virtualenv env
. env/bin/activate
```

### install required packages:
```bash
pip install -r requirements.txt
```

### create database and give permissions to use it
Use the same as defined in config.py

### install database tables
```bash
./manage.py db upgrade
```
### Create User
```bash
./manage.py shell
>>> User.generate_admin_account(email='you@domain.com',username='Yourusername',password='mysecret')
>>> quit()
```

## for development (Not recommended for production!)

### run embedded webserver
```bash
./manage.py runserver
```

## for production

I prefer nginx as reverse proxy and uwsgi to run this project


see [init folder](/init/)