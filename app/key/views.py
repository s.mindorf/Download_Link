# -*- coding: utf-8 -*-
"""
Copyright 2015 Soeren Mindorf <support@mit42.de> and Fabian Zoske <support@zoske.it>

This file is part of Download-Link.

Download-Link is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

Download-Link is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Download-Link. If not, see <http://www.gnu.org/licenses/>.
"""

from datetime import datetime, timedelta
from random import SystemRandom

from flask import render_template, send_from_directory, current_app, url_for, redirect, flash, request
from flask.ext.babel import gettext
from flask.ext.login import login_required
from sqlalchemy.exc import IntegrityError
from . import key
from .forms import Download, DownloadWithoutCaptcha
from ..model import db, DownloadKey

random = SystemRandom()


@key.route('/', methods=['GET', 'POST'])
@key.route('/<form_key>', methods=['GET', 'POST'])
def download_site(form_key=''):
    if current_app.config['RECAPTCHA_ENABLE']:
        form = Download(download_key=form_key)
    else:
        form = DownloadWithoutCaptcha(download_key=form_key)
    if form.validate_on_submit():
        download_key = form.download_key.data
        now = datetime.utcnow()
        directory = current_app.config['UPLOAD_FOLDER']
        db_file = DownloadKey.query.filter_by(download_key=download_key).first()
        if db_file:
            filename = db_file.file.filename
            attachment_name = db_file.file.downloadName
            db_time = db_file.timestamp + timedelta(db_file.lifetime)
            if db_time >= now:
                if db_file.downloads < db_file.maxdownloads:
                    db_file.downloads += 1
                if db_file.downloads == db_file.maxdownloads:
                    db.session.delete(db_file)
                try:
                    db.session.commit()
                except IntegrityError:
                    db.session.rollback()
            else:
                flash(gettext(u'The lifetime of the downloads was reached!'))
                return redirect(url_for('key.download_site'))
        else:
            flash(gettext(u'The entered key was invalid!'), 'error')
            return redirect(url_for('key.download_site'))
        return send_from_directory(directory=directory, filename=filename, as_attachment=True,
                                   attachment_filename=attachment_name)
    return render_template('key/download.html', title=gettext(u'Download'), form=form)


@key.route('/<int:key_id>/delete')
@login_required
def delete_key(key_id):
    file_id = request.args.get('file_id')
    entry = DownloadKey.query.get(key_id)
    if entry:
        db.session.delete(entry)
        try:
            db.session.commit()
        except IntegrityError:
            db.session.rollback()
        flash(gettext(u'Entry deleted: ') + str(key_id))
        return redirect(url_for('file.show_uploads', file_id=file_id))
    else:
        flash(gettext(u'The key is already deleted, please refresh the site.'))
        return redirect(url_for('file.show_uploads', file_id=file_id))
