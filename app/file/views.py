# -*- coding: utf-8 -*-
"""
Copyright 2015 Soeren Mindorf <support@mit42.de> and Fabian Zoske <support@zoske.it>

This file is part of Download-Link.

Download-Link is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

Download-Link is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Download-Link. If not, see <http://www.gnu.org/licenses/>.
"""


import os
from app import babel, nav
from config import LANGUAGES
from flask import render_template, redirect, request, url_for, flash, current_app
from flask.ext.babel import gettext, lazy_gettext
from flask.ext.login import login_required
from flask_nav.elements import Navbar, View, Subgroup, Text
from sqlalchemy.exc import IntegrityError
from werkzeug.utils import secure_filename
from . import file
from .forms import FileEditForm, GenerateUid, UploadForm
from ..model import db, File, DownloadKey
from ..utils.helper import get_random_string


topbar = Navbar(
    View((lazy_gettext(u'Home')), 'file.management'),
    Subgroup(lazy_gettext(u'Maintenance'),
             Text('Database tasks'),
             View(lazy_gettext(u'Cleanup Database if file exist on filesystem'), 'maintenance.cleanup_file_in_db'),
             View(lazy_gettext(u'Delete files where lifetime is exceeded'), 'maintenance.cleanup_file_lifetime')),
    Subgroup(lazy_gettext(u'Your Account'),
             View(lazy_gettext(u'Change Password'), 'user.change_password'),
             View(lazy_gettext(u'Logout'), 'auth.logout')),
    Subgroup(lazy_gettext(u'About'),
             View(lazy_gettext(u'Impress'), 'main.about'),
             View(lazy_gettext(u'License'), 'main.license')),
    View((lazy_gettext(u'Help')), 'main.help')
)

nav.register_element('top', topbar)


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in current_app.config['ALLOWED_EXTENSIONS']


@babel.localeselector
def get_locale():
    return request.accept_languages.best_match(LANGUAGES.keys())


@file.route('/management', methods=['GET', 'POST'])
@file.route('/management/<int:page>', methods=['GET', 'POST'])
@login_required
def management(page=1):
    form = UploadForm(request.form)
    form_uid = GenerateUid()
    form_edit = FileEditForm()
    get_files = File.query.order_by(File.id)
    files = get_files.paginate(page, current_app.config['IDS_PER_PAGE'], False)
    return render_template('file/filemanager.html', files=files, form=form, form_uid=form_uid, form_edit=form_edit,
                           title=gettext(u'Filemanager'))


@file.route('/<int:file_id>/gen_uid', methods=['GET', 'POST'])
@login_required
def gen_uid(file_id):
    form = GenerateUid()
    if form.validate_on_submit():
        db_file = File.query.get_or_404(file_id)
        max_downloads, genuniqueids, lifetime = (form.max_downloads.data, form.genuniqueids.data, form.lifetime.data)
        if not lifetime:
            lifetime = 10
        if lifetime < 1 or lifetime > 120:
            flash(gettext(u'Please enter a number between 1 and 120!'))
            return redirect(url_for('file.management'))
        show_key = []
        for _ in range(0, genuniqueids):
            key = DownloadKey(download_key=get_random_string(), filename_id=file_id, maxdownloads=max_downloads,
                           lifetime=lifetime)
            upd_file = File.query.filter_by(id=file_id).first()
            upd_file.lifetime = lifetime
            db.session.add(key)
            try:
                db.session.commit()
            except IntegrityError:
                db.session.rollback()
            show_key.append(key)
        host = request.host_url
        return render_template('file/show_uids.html', form=form, uids=show_key, file=db_file, host=host,
                               title=gettext(u'Unique IDs'))
    flash(gettext(u'Your entry was not correct!'))
    return render_template("file/show_uids.html", form=form, title=gettext(u'Unique IDs'))


@file.route('/<int:file_id>/edit', methods=['GET', 'POST'])
@login_required
def edit(file_id):
    db_file = File.query.get_or_404(file_id)
    form = FileEditForm(obj=db_file)
    if form.validate_on_submit():
        db_file.note, db_file.downloadName, db_file.lifetime = (
            form.note.data, form.downloadName.data.replace(' ', '_'), form.lifetime.data)
        db.session.add(db_file)
        try:
            db.session.commit()
        except IntegrityError:
            db.session.rollback()
        flash(u'Database entry updated!')
        return redirect(url_for('file.management'))
    return render_template('file/upload.html', form=form)


@file.route('/<int:file_id>/delete')
@login_required
def delete(file_id):
    db_file = File.query.get_or_404(file_id)
    fs_file = os.path.join(current_app.config['UPLOAD_FOLDER'], db_file.filename)
    if os.path.exists(current_app.config['UPLOAD_FOLDER']):
        if os.path.isfile(fs_file):
            db.session.delete(db_file)
            os.remove(fs_file)
            try:
                db.session.commit()
            except IntegrityError:
                db.session.rollback()
            flash(gettext(u'File deleted: ') + db_file.filename)
        else:
            flash(gettext(u'File not found!'))
    return redirect(url_for('file.management'))


@file.route('/upload', methods=['GET', 'POST'])
@login_required
def upload():
    form = UploadForm(request.form)
    if form.validate_on_submit():
        filename, note, download_name, lifetime = (form.filename.data, form.note.data, form.download_name.data,
                                                   form.lifetime.data)
        file = request.files['filename']
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            if not os.path.isfile(os.path.join(current_app.config['UPLOAD_FOLDER'], filename)):
                with open(os.path.join(current_app.config['UPLOAD_FOLDER'], filename), 'wb+') as dest:
                    for chunk in filename.chunks():
                        dest.write(chunk)
                # file.save(os.path.join(current_app.config['UPLOAD_FOLDER'], filename))
                if not download_name:
                    download_name = filename
                download_name.replace(' ', '_')
                ins_files = File(downloadName=download_name, note=note, filename=filename, lifetime=lifetime)
                db.session.add(ins_files)
                try:
                    db.session.commit()
                except IntegrityError:
                    db.session.rollback()
            else:
                flash(gettext(u'File already exists!'))
            return redirect(url_for('file.management', filename=filename))
        else:
            flash(gettext(u'Your filename extension is not allowed!'))
            return redirect(url_for('file.management'))
    return render_template('file/upload.html', form=form, title=gettext(u'Edit'))


@file.route('/show_uploads', methods=['GET', 'POST'])
@file.route('/<int:file_id>/show_uploads', methods=['GET', 'POST'])
@file.route('/<int:file_id>/show_uploads/<int:page>', methods=['GET', 'POST'])
@login_required
def show_uploads(file_id=0, page=1):
    if request.method == 'POST':
        if request.form['action'] == 'delete_entry':
            entry = DownloadKey.query.filter_by(download_key=request.form['id']).first()
            db.session.delete(entry)
            try:
                db.session.commit()
            except IntegrityError:
                db.session.rollback()
            flash(gettext(u'Database entry deleted: ') + request.form['id'])
            return redirect(url_for('file.show_uploads', file_id=entry.filename_id))
        return redirect(url_for('file.show_uploads'))
    entry1 = DownloadKey.query.filter_by(filename_id=file_id)
    unique_keys = entry1.paginate(page, current_app.config['IDS_PER_PAGE'], False)
    host = request.host_url
    return render_template('file/show_entries.html', download_keys=unique_keys, f_id=file_id,
                           title=gettext(u'Uploaded files'), host=host)
