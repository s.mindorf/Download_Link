# -*- coding: utf-8 -*-
"""
Copyright 2015 Soeren Mindorf <support@mit42.de> and Fabian Zoske <support@zoske.it>

This file is part of Download-Link.

Download-Link is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

Download-Link is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Download-Link. If not, see <http://www.gnu.org/licenses/>.
"""

from flask.ext.babel import lazy_gettext
from flask.ext.wtf import Form
from wtforms import StringField, SubmitField, IntegerField, HiddenField
from flask_wtf.file import FileField
from wtforms.validators import Length, number_range, DataRequired


class UploadForm(Form):
    filename = FileField(lazy_gettext(u'Upload file:'))
    download_name = StringField(lazy_gettext(u'Filename to download:'), [Length(max=50)])
    note = StringField(lazy_gettext(u'Note:'), [Length(max=255)])
    lifetime = IntegerField(lazy_gettext(u'Lifetime (in days):'), default=60)
    submit = SubmitField(lazy_gettext(u'Upload'))


class GenerateUid(Form):
    max_downloads = IntegerField(lazy_gettext(u'Enter maximal count of Downloads:'),
                                 [number_range(min=1, max=99), DataRequired()])
    genuniqueids = IntegerField(lazy_gettext(u'How many Downloadkeys should be generated (1-40)?'),
                                [number_range(min=1, max=40),
                                 DataRequired()])
    lifetime = IntegerField(lazy_gettext(u'How long should the Download be valid (in days)? '
                                         u'(default=10, Max: 90 days)'), default=10)
    file_id = HiddenField(id='file_id')
    submit = SubmitField(lazy_gettext(u'generate uniqueids'))


class FileEditForm(Form):
    downloadName = StringField(lazy_gettext(u'Filename to download'), [Length(max=50)])
    note = StringField(lazy_gettext(u'Note'), [Length(max=255)])
    lifetime = IntegerField(lazy_gettext(u'Lifetime (in days)'), default=120)
    submit = SubmitField(lazy_gettext(u'Update'))
