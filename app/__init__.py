"""
Copyright 2015 Soeren Mindorf <support@mit42.de> and Fabian Zoske <support@zoske.it>

This file is part of Download-Link.

Download-Link is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

Download-Link is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Download-Link. If not, see <http://www.gnu.org/licenses/>.
"""

from config import config
from flask import Flask
from flask.ext.babel import Babel, lazy_gettext
from flask.ext.bcrypt import Bcrypt
from flask.ext.bootstrap import Bootstrap
from flask.ext.login import LoginManager
from flask.ext.mail import Mail
from flask.ext.moment import Moment
from flask.ext.sqlalchemy import SQLAlchemy
from flask.json import JSONEncoder
from flask_nav import Nav


class CustomJSONEncoder(JSONEncoder):
    """This class adds support for lazy translation texts to Flask's
    JSON encoder. This is necessary when flashing translated texts."""

    def default(self, obj):
        from speaklater import is_lazy_string
        if is_lazy_string(obj):
            try:
                return unicode(obj)  # python 2
            except NameError:
                return str(obj)  # python 3
        return super(CustomJSONEncoder, self).default(obj)


bootstrap = Bootstrap()
mail = Mail()
moment = Moment()
db = SQLAlchemy()
babel = Babel()
nav = Nav()
bcrypt = Bcrypt()

login_manager = LoginManager()
login_manager.session_protection = 'strong'
login_manager.login_view = 'auth.login'
login_manager.login_message = lazy_gettext(u'Please log in to access this page.')


def create_app(config_name):
    app = Flask(__name__)
    app.config.from_object(config[config_name])
    app.json_encoder = CustomJSONEncoder
    config[config_name].init_app(app)

    bootstrap.init_app(app)
    mail.init_app(app)
    moment.init_app(app)
    db.init_app(app)
    login_manager.init_app(app)
    babel.init_app(app)
    nav.init_app(app)
    bcrypt.init_app(app)

    from .main import main as main_blueprint
    app.register_blueprint(main_blueprint)

    from .auth import auth as auth_blueprint
    app.register_blueprint(auth_blueprint, url_prefix='/auth')

    from .file import file as file_blueprint
    app.register_blueprint(file_blueprint, url_prefix='/file')

    from .key import key as key_blueprint
    app.register_blueprint(key_blueprint, url_prefix='/key')

    from .maintenance import maintenance as maintenance_blueprint
    app.register_blueprint(maintenance_blueprint, url_prefix='/maintenance')

    from .user import user as user_blueprint
    app.register_blueprint(user_blueprint, url_prefix='/user')
    return app
