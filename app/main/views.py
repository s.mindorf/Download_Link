# -*- coding: utf-8 -*-
"""
Copyright 2015 Soeren Mindorf <support@mit42.de> and Fabian Zoske <support@zoske.it>

This file is part of Download-Link.

Download-Link is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

Download-Link is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Download-Link. If not, see <http://www.gnu.org/licenses/>.
"""

from flask import render_template, redirect, url_for
from flask.ext.babel import gettext
from flask.ext.login import login_required, current_user
from . import main


@main.route('/')
def index():
    if current_user.is_authenticated:
        return redirect(url_for('file.management'))
    return render_template('main/index.html', title=gettext(u'Welcome'))


@main.route('/menu')
@login_required
def menu():
    return render_template('main/menu.html', title=gettext(u'Mainmenu'))


@main.route('/about')
@login_required
def about():
    return render_template('main/about.html', title=gettext(u'About'))


@main.route('/license')
@login_required
def license():
    return render_template('main/license.html',
                           title='GNU General Public License v3.0 - GNU Project - Free Software Foundation (FSF)')


@main.route('/help')
@login_required
def help():
    return render_template('main/help.html', title=gettext(u'Help'))

