"""
Copyright 2015 Soeren Mindorf <support@mit42.de> and Fabian Zoske <support@zoske.it>

This file is part of Download-Link.

Download-Link is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

Download-Link is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Download-Link. If not, see <http://www.gnu.org/licenses/>.
"""

from flask.ext.babel import lazy_gettext
from flask.ext.wtf import Form
from wtforms import StringField, PasswordField, validators


class RegistrationForm(Form):
    username = StringField(lazy_gettext(u'Username:'), [validators.Length(min=4, max=25)])
    email = StringField(lazy_gettext(u'Email address:'), [validators.Length(min=6, max=35), validators.Email("EMAIL")])
    password = PasswordField(lazy_gettext(u'Password:'),
                             [validators.DataRequired(), validators.EqualTo('confirm', message='Passwords must match')])
    confirm = PasswordField(lazy_gettext(u'repeat Password:'))



