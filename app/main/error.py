"""
Copyright 2015 Soeren Mindorf <support@mit42.de> and Fabian Zoske <support@zoske.it>

This file is part of Download-Link.

Download-Link is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

Download-Link is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Download-Link. If not, see <http://www.gnu.org/licenses/>.
"""

from flask import render_template
from . import main


@main.app_errorhandler(404)
def page_not_found(_):
    return render_template('error/404.html'), 404


@main.app_errorhandler(405)
def page_not_found(_):
    return render_template('error/405.html'), 405


@main.app_errorhandler(413)
def page_not_found(_):
    return render_template('error/413.html'), 413


@main.app_errorhandler(500)
def internal_server_error(_):
    return render_template('error/500.html'), 500
