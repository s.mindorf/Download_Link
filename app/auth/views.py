"""
Copyright 2015 Soeren Mindorf <support@mit42.de> and Fabian Zoske <support@zoske.it>

This file is part of Download-Link.

Download-Link is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

Download-Link is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Download-Link. If not, see <http://www.gnu.org/licenses/>.
"""

from flask import render_template, redirect, request, url_for, flash
from flask.ext.babel import gettext
from flask.ext.login import login_user, logout_user, login_required
from . import auth
from .forms import LoginForm
from ..model import User


@auth.route('/login', methods=['GET', 'POST'])
def login():
    # TODO: CAPTCHA
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user and user.verify_password(form.password.data):
            login_user(user)
            return redirect(request.args.get('next') or url_for('file.management'))
        flash(gettext(u'Wrong username or password!'))
    return render_template('auth/login.html', form=form, title=gettext(u'Sign In'))


@auth.route('/logout')
@login_required
def logout():
    logout_user()
    flash(gettext(u'you where successfully logged out!'))
    return redirect(url_for('main.index'))
