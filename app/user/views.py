# -*- coding: utf-8 -*-
"""
Copyright 2015 Soeren Mindorf <support@mit42.de> and Fabian Zoske <support@zoske.it>

This file is part of Download-Link.

Download-Link is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

Download-Link is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Download-Link. If not, see <http://www.gnu.org/licenses/>.
"""

from flask import render_template, url_for, redirect, flash
from flask.ext.babel import gettext
from flask.ext.login import login_required, current_user
from sqlalchemy.exc import IntegrityError
from . import user
from .forms import PasswordChange
from ..model import db


@user.route('/change_password', methods=['GET', 'POST'])
@login_required
def change_password():
    form = PasswordChange()
    if form.validate_on_submit():
        old_password = form.old_password.data
        new_password = form.new_password.data
        if current_user.verify_password(old_password):
            current_user.password = new_password
            db.session.add(current_user)
            try:
                db.session.commit()
            except IntegrityError:
                db.session.rollback()
            flash(gettext(u'Password updated!'))
            return redirect(url_for('file.management'))
        else:
            flash(gettext(u'Old Password did not match!'))
            return redirect(url_for('file.management'))
    return render_template('user/password_change.html', form=form, title=gettext(u'Change Password'))
