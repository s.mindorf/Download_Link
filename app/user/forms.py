"""
Copyright 2015 Soeren Mindorf <support@mit42.de> and Fabian Zoske <support@zoske.it>

This file is part of Download-Link.

Download-Link is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

Download-Link is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Download-Link. If not, see <http://www.gnu.org/licenses/>.
"""

from flask.ext.babel import lazy_gettext
from flask.ext.wtf import Form
from wtforms import SubmitField, PasswordField
from wtforms.validators import DataRequired, EqualTo


class PasswordChange(Form):
    old_password = PasswordField(lazy_gettext(u'Please enter your old Password'), [DataRequired()])
    new_password = PasswordField(lazy_gettext(u'Please enter your new Password'),
                                 [DataRequired(), EqualTo('confirm', message=lazy_gettext(u'Passwords must match'))])
    confirm = PasswordField(lazy_gettext(u'Repeat Password'))
    submit = SubmitField(lazy_gettext(u'update'))
