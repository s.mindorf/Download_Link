# -*- coding: utf-8 -*-
"""
Copyright 2015 Soeren Mindorf <support@mit42.de> and Fabian Zoske <support@zoske.it>

This file is part of Download-Link.

Download-Link is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

Download-Link is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Download-Link. If not, see <http://www.gnu.org/licenses/>.
"""

import os
from datetime import datetime, timedelta
from flask import redirect, url_for, flash, current_app
from flask.ext.babel import gettext
from flask.ext.login import login_required
from sqlalchemy.exc import IntegrityError
from . import maintenance
from ..model import db, File


@maintenance.route('/cleanup_file_in_db')
@login_required
def cleanup_file_in_db():
    filenames = File.query.all()
    if filenames:
        for row in filenames:
            if not os.path.isfile(os.path.join(current_app.config['UPLOAD_FOLDER'], row.filename)):
                db_file = File.query.get_or_404(row.id)
                db.session.delete(db_file)
                try:
                    db.session.commit()
                    flash(gettext(u'Database cleanup for deleted files successfully!'))
                except IntegrityError:
                    db.session.rollback()
                    flash(gettext(u'Database cleanup for deleted files unsuccessful!'))
                return redirect(url_for('file.management'))
    flash(gettext(u'No database entries found to clean!'))
    return redirect(url_for('file.management'))


@maintenance.route('/cleanup_file_lifetime')
@login_required
def cleanup_file_lifetime():
    now = datetime.utcnow()
    filenames = File.query.all()
    if filenames:
        for row in filenames:
            db_time = row.uploaddate + timedelta(row.lifetime)
            if db_time <= now:
                try:
                    os.remove(os.path.join(current_app.config['UPLOAD_FOLDER'], row.filename))
                except:
                    flash(gettext(u'Unable to delete File'))
                db_file = File.query.get_or_404(row.id)
                db.session.delete(db_file)
                try:
                    db.session.commit()
                    flash(gettext(u'Deleted all files where lifetime is exceeded'))
                except IntegrityError:
                    db.session.rollback()
                    flash(gettext(u'Could not delete files, please check manually'))
            flash(gettext(u'No database entries where lifetime is exceeded!'))
            return redirect(url_for('file.management'))
    flash(gettext(u'No database entries found to clean!'))
    return redirect(url_for('file.management'))
