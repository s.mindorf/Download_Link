"""
Copyright 2015 Soeren Mindorf <support@mit42.de> and Fabian Zoske <support@zoske.it>

This file is part of Download-Link.

Download-Link is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

Download-Link is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Download-Link. If not, see <http://www.gnu.org/licenses/>.
"""

from datetime import datetime

from flask.ext.login import UserMixin
from . import db, login_manager, bcrypt

__author__ = 's.mindorf'


class Role(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), unique=True)
    default = db.Column(db.Boolean, default=False, index=True)
    permissions = db.Column(db.Integer)
    users = db.relationship('User', backref='role', lazy='dynamic')

    def __repr__(self):
        return 'Role %r' % self.name


class File(db.Model):
    id = db.Column(db.Integer, unique=True, autoincrement=True, primary_key=True)
    filename = db.Column(db.String(60))
    note = db.Column(db.String(255))
    downloadName = db.Column(db.String(255))
    uploaddate = db.Column(db.DateTime, default=datetime.utcnow)
    lifetime = db.Column(db.Integer)
    unique_id = db.relationship('DownloadKey', cascade='delete,all', backref='DownloadKey', lazy='dynamic')

    def __repr__(self):
        return 'Download %r' % self.filename


class DownloadKey(db.Model):
    id = db.Column(db.Integer, primary_key=True, unique=True, autoincrement=True)
    filename_id = db.Column(db.Integer, db.ForeignKey('file.id'))
    timestamp = db.Column(db.DateTime, default=datetime.utcnow)
    lifetime = db.Column(db.Integer)
    maxdownloads = db.Column(db.Integer)
    downloads = db.Column(db.Integer, default=0)
    download_key = db.Column(db.String(16), unique=True)
    recipient = db.Column(db.String(200))
    downloadpassword_hash = db.Column(db.String(128))
    file = db.relationship('File')


class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    email = db.Column(db.String(120), index=True, unique=True)
    password_hash = db.Column(db.String(128))
    role_id = db.Column(db.Integer, db.ForeignKey('role.id'))

    def __init__(self, username, email, password):
        self.username = username
        self.email = email
        self.password = password

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        try:
            return unicode(self.id)  # python 2
        except NameError:
            return str(self.id)  # python 3

    def __repr__(self):
        return '<User %r>' % self.username

    @staticmethod
    def generate_admin_account(email='soeren@example.com', username='soeren', password='Pa$$w0rd'):
        from sqlalchemy.exc import IntegrityError
        u = User(email=email, username=username, password=password)
        db.session.add(u)
        try:
            db.session.commit()
        except IntegrityError:
            db.session.rollback()

    @property
    def password(self):
        raise AttributeError('password is not a readable attribute')

    @password.setter
    def password(self, password):
        self.password_hash = bcrypt.generate_password_hash(password)

    def verify_password(self, password):
        return bcrypt.check_password_hash(self.password_hash, password)


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))
