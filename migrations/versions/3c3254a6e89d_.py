"""empty message

Revision ID: 3c3254a6e89d
Revises: None
Create Date: 2016-04-21 14:49:41.948073

"""

# revision identifiers, used by Alembic.
revision = '3c3254a6e89d'
down_revision = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.create_table('file',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('filename', sa.String(length=60), nullable=True),
    sa.Column('note', sa.String(length=255), nullable=True),
    sa.Column('downloadName', sa.String(length=255), nullable=True),
    sa.Column('uploaddate', sa.DateTime(), nullable=True),
    sa.Column('lifetime', sa.Integer(), nullable=True),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('id')
    )
    op.create_table('role',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('name', sa.String(length=64), nullable=True),
    sa.Column('default', sa.Boolean(), nullable=True),
    sa.Column('permissions', sa.Integer(), nullable=True),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('name')
    )
    op.create_index(op.f('ix_role_default'), 'role', ['default'], unique=False)
    op.create_table('DownloadKey',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('filename_id', sa.Integer(), nullable=True),
    sa.Column('timestamp', sa.DateTime(), nullable=True),
    sa.Column('lifetime', sa.Integer(), nullable=True),
    sa.Column('maxdownloads', sa.Integer(), nullable=True),
    sa.Column('downloads', sa.Integer(), nullable=True),
    sa.Column('download_key', sa.String(length=16), nullable=True),
    sa.Column('recipient', sa.String(length=200), nullable=True),
    sa.Column('downloadpassword_hash', sa.String(length=128), nullable=True),
    sa.ForeignKeyConstraint(['filename_id'], ['file.id'], ),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('download_key'),
    sa.UniqueConstraint('id')
    )
    op.create_table('user',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('username', sa.String(length=64), nullable=True),
    sa.Column('email', sa.String(length=120), nullable=True),
    sa.Column('password_hash', sa.String(length=128), nullable=True),
    sa.Column('role_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['role_id'], ['role.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_index(op.f('ix_user_email'), 'user', ['email'], unique=True)
    op.create_index(op.f('ix_user_username'), 'user', ['username'], unique=True)
    ### end Alembic commands ###


def downgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.drop_index(op.f('ix_user_username'), table_name='user')
    op.drop_index(op.f('ix_user_email'), table_name='user')
    op.drop_table('user')
    op.drop_table('uniqueID')
    op.drop_index(op.f('ix_role_default'), table_name='role')
    op.drop_table('role')
    op.drop_table('file')
    ### end Alembic commands ###
